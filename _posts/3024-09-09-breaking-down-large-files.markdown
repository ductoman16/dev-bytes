---
layout: post
title:  "Breaking down large files"
date: "3024-09-09"
categories: refactoring
---

## Refactoring Large Files

My Process:

- Low hanging fruit (anything that's static, small, and obviously doesn't belong in the class)
  - static methods (ones that could be extension methods?)
  - Easy SRP wins: validation
  - Methods with no dependencies that take a POCO parameter (put the method on the poco where appropriate!)
- CQS

_The goal is to make everything low-hanging fruit eventually!_

### Break down methods

#### CQS

#### Move methods onto parameters

### Move into separate classes

#### Cohesion
