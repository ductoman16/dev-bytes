---
layout: post
title:  "Hello, World!"
date: "2024-09-09"
---

## Welcome to the DevBytes blog

Hi! This is a blog where I'm going to write about software engineering stuff.

For some background: my name is Ryan, and I'm a software engineer! For the past 10+ years, I've been mainly working on .NET enterprise apps - so the advice and opinions in this blog will mostly be targeted towards that environment and relevant technologies. I like to dabble in plenty of other technologies though, so other topics may appear from time to time!
