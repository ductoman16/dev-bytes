---
layout: post
title:  "Error Message Wall of Shame"
date: "2024-09-30"
categories: errors
---

## Error Message Wall of Shame

Here's my collection of bad error messages. These are examples of what _not_ to do when writing error messages, with details about exactly what is wrong with each of them!

### Google Home - No Internet Connection

![No Internet Connection](/images/error-message-wall-of-shame/no_internet_connection.jpg)

If I have no internet connection, how did I take this screenshot and upload it to my blog?

I got this while trying to add a new device to my Google Home. Why? No idea!

- ❌ No cause
- ❌ No potential fixes
- ❌ Lying

If you're going to tell me I have no internet, you better be sure of that. Maybe they meant it couldn't connect to a their server?
