---
layout: post
title:  "Testing Without Mocks"
date: "3024-08-21"
categories: testing
---

## Testing Without Mocks

This post is a summary of James Shore's excellent strategy for testing without mocks, and you can find all of his original resources at the following link:

<https://www.jamesshore.com/v2/projects/nullables>

After using this strategy for a short while, this is now my defacto testing strategy because it addresses _just about every_ issue I've had with unit testing in the past. This is the strategy that I recommend using - regardless of what language you work with.

While James Shore's writing on the topic is very thorough, it's also very dense, and there are a few terms that I feel could use additional disambiguation, so I will attempt to address that here.

## My Problem With Unit Tests

We've all seen tests that look like this. I know I've written many of them!

![The Standard .NET Unit Test](../images/testing-without-mocks/StandardNetUnitTest.png)

This is pretty much the standard for .NET unit tests.
My problem with tests like this is that it really only tests **that the code that you wrote, is the code that you wrote**.

Tests like this have a few advantages: they're simple, logical, and isolated!

However, they also have a few disadvantages. They tend to lock in your implementation - if you decide to change how the method is implemented, your code might still work, but the test may fail because you're no longer verifying the right method on the mock.

These kinds of tests also aren't a very good representation of how code is used in production. After all, the best unit tests can also serve as documentation!

The final issue with these kinds of tests is that they can miss bugs in behavior between components. This typically means that you'll need a suite of end-to-end tests to test how all of the components work when wired together (but you probably already knew that).

###

## **** ScratchPad ****

<https://www.jamesshore.com/v2/projects/nullables/testing-without-mocks>

Things I've learned from the Testing Without Mocks course:

Solitary vs Sociable

State based vs Interaction based

Broad vs Narrow tests

Integration vs Unit

"Solitary tests pass when they should fail"
"Interaction based tests fail when they should pass"

"Only stub out code you don't own [(3rd party)]"

"Spies track function calls, output tracking tracks behavior"

Bonus that I noticed: Creating a thin wrapper in C# has the benefit of making extension methods testable/verifieable. They end up as normal methods on your wrapper. Extension methods don't usually play nice with mocking libraries.

Overlapping tests? They overlap running code, but they each only assert their own layer's functionality.

- __Q__: Why hand-write stubs instead of just using a mocking library for your low-level infrastructure stubs?
    - __A__: One answer might be that using less libraries is better? I'm sure there are other reasons. TODO
- __Q__: What do you do if the class you put the `createNull` factory method on takes other dependencies?
    - __A__: Prefer _not_ using dependency injection, and just instantiate the dependencies in the factory method. You can use dependency injection if you must, but it can facilitate bad design by encouraging deeply nested dependency trees.
- __Q__: Why use static factory methods instead of constructors?
- __Q__: Output trackers are weird.
    - __A__: Output trackers are like a spy, but they track domain behavior, not function calls. They are tied to the actual external call they're meant to track by the a integration tests, ensuring one doesn't happen without the other.
- Q: Should the embedded stub behave _exactly_ like the real dependency except external calls? Isn't that a lot to implement in some cases like DBs?
- Q: Why do output trackers go in the infrastructure wrappers and not in the Thin Wrappers?
- Q: In a microservice architecture, it seems like the integration tests for the infrastructure of an upstream service will duplicate the logic tested by the tests of the underlying downstream microservice. Is this ok?
    - A: Maybe? If you don't own the downstream service, it makes sense to verify that you are calling it so that it can do its job correctly. I think the same applies if you do happen to own the downstream service. You're not verifying that the downstream service is working correctly, you're verifying that you can call it successfully and in an expected manner.

__Nullable__: Any class that has infrastructure in its dependency chain should be able to be made nullable

__Embedded Stubs__: Only classes that directly use 3rd party dependencies should contain an embedded stub

__Infrastructure wrapper__: Directly uses a 3rd party dependency. Test it with Narrow Integration Tests, then make it Nullable with an Embedded Stub 

- Note: Classes shouldn't be an infrastructure wrapper as well as having other dependencies

Types of classes:
A: Pure logic - no infrastructure dependencies `[InfrastructureDependencies.None]`
B: Infrastructure Wrappers - have direct 3rd party dependencies `[InfrastructureDependencies.Direct]`
C: Indirect Infrastructure - depend on Infrastructure wrappers `[InfrastructureDependencies.Indirect]`