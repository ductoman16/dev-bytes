---
layout: post
title:  "Anatomy of a Good Error Message"
date: "2024-09-10"
categories: errors
---

As a software engineer, there's nothing that frustrates me as much as an unhelpful error message. Far too often I find error messages that are too generic, too cryptic, or just plain misleading. That's the inspiration behind this post, and I'm going to try and define a clear set of rules that every software engineer should follow when writing error messages.

#### A note about audiences

One of the most important factors in any writing is knowing your audience, and writing error messages is no different. Obviously, errors intended for software engineers will be very different than errors intended for the general public, so make sure you're using language that is appropriate for your target audience!

This article will be primarily focused on error messages for software engineers and other highly technical users, however, I do think that these 4 points can also be applied to error messages written for non-technical users (with audience appropriate detail levels and word choice)!

---

## **Anatomy of a good error message**

Alright, here it is.
Every good error message should have the following:

1. **What the issue is _(and where, if relevant)_**

2. **What the cause(s) might be**

3. **What you can try to fix each of those causes**

4. [_Optional_] **Links to relevant online info if appropriate**

Let's explore each of these a little bit:

### 1: What the issue is

This should be the actual problem that the current code is running into. What is the program trying to do that it can't?

If applicable, include a location, such as a line number.

//TODO: Add context for when you get an error out of the blue. "'Could not connect to server?' But I wasn't trying to connect to a server!"

### 2: What the cause(s) might be

This is the "why" of the error message. If you know exactly why the error occurred, great! Put it here!

Sometimes you don't know exactly what caused an error, and that's ok. Maybe the details got swallowed in some code you don't have access to, or maybe there were no details to begin with. However, if you've tested your code, you will probably know at least a few reasons why this error could happen, and you should list them as _potential_ causes.

Remember to make it obvious if the error is caused by something the consumer did, or if it's out of their control.

### 3: What to try

If you know what caused the error, _you_ might know how to fix it - but there's a good chance the person reading your error message doesn't! (even if that person is you in 5 years)

If there are multiple potential causes, make sure to list potential fixes for all of them.

If the error is something that's out of your control (external service is down, etc), this would be a good place to provide a workaround! "Try again later" should be avoided if at all possible.

### 4: Links to relevant online info if appropriate

If you want to link to something online, remember that websites are not forever. The error message itself should tell you what to do to fix the problem, and it should be something that is searchable if you the recipient doesn't know how to complete that task. That is to say - your specific link can be helpful, but should not be required.

---

## Pro tips

- **Include lots of details!** If you're working with a file, tell me what file. If you're operating on some data, tell me what data you actually received, and what you expected (I basically already said this in points 1 and 2, but actual/expected is a very good format to present that data).

- **Wrap variables with quotes**, so you can easily tell if they're empty.

## Don't do this

- **Don't lie in your error messages.** Obviously mistakes can happen, but make sure you're not including incorrect information in your error messages.

- **Don't hide information.** "Oops, something went wrong" is not a good error message, no matter the audience.

## What about **fallback error messages?**

At some point, you will need to write an error message for a case you haven't handled. In this case, you should still know _what_ the issue is (due to exception information or otherwise), but maybe not why, or how to fix it. I have two suggestions for this scenario:

1. Thorough automated testing! This will help you minimize the number of unknown cases you have.
2. Observability: if you're fortunate enough to be writing code where you can log the errors, make sure you're logging these unhandled cases. Once you know about them, you can turn them into handled cases and write good error messages!

## That's it

Four things! That's all it takes to write a good error message.

1. **What**
2. **Why**
3. **What to do about it**
4. **Optional further reading**

 While I can't guarantee that this will let you make _perfect_ error messages, I can assure you that they will be **good**.
