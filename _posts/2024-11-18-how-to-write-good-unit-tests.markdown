---
layout: post
title:  "How To Write Good Unit Tests"
date: "3024-11-18"
categories: testing
---

## Intro

I've had an interesting relationship with unit tests throughout my career. When I first started out, I loved the idea, and wanted to write lots of unit tests. Then once I started writing unit tests, I didn't like how I was writing them (very tightly coupled to implementation), and decided it was too much work for not enough benefit. I did however like how unit tests forced you to create modular and reusable code, so I thought, "maybe you can just write code as if you were going to unit test it, and then skip the tests to save time". Spoiler alert: that only works for so long. After a lot more practice and successfully using unit tests to wrangle unruly legacy codebases, I now swear by unit tests and won't go without them again!

This article is going to contain my definitive advice for writing the best unit tests, based on my 10+ years of experience on the subject.

I've formatted this as a set of recommended best practices, as well as a reason for every one so you can understand the "why" and determine if that reason makes sense in your particular scenario.

My experience is primarily in the .NET and JS/TS stacks, but between the xUnit-style frameworks and mocha/jasmine-style frameworks, I believe this advice should be broadly applicable to most commonly used modern testing frameworks. I've included technology/framework-specific advice where applicable.

## Conventional Advice

First, let's take a look at some of the conventional best practices you'll encounter when first learning about unit tests: 

### FIRST principles

✅ **Recommendation**: Follow the FIRST principles, but create Independent, not Isolated tests.

🔎 **Reason**:

TL;DR: Your tests need to be able to pass independently, but it doesn't matter if they fail together!

The first principles are usually defined as follows:

- **Fast**: Unit tests should be (extremely) fast
- **Independent/Isolated**: One unit test should not depend on the result produced by another unit test. 👈 (This is the one I take issue with)
- **Repeatable**: If run multiple times, a unit test should always produce the same result.
- **Self-validating**: The test should not require any additional steps to determine if it passed or failed.
- **Timely/Thorough**: **Timely** usually refers to creating the tests when you create the production code (I highly recommend practicing TDD). **Thorough** of course means that your tests should cover all possible scenarios. I find mutation testing tools like [Stryker Mutator](https://stryker-mutator.io/) invaluable for this.

Why do I take issue with the independent/isolated advice? There are typically two ways that it's interpreted. (1) The tests should not need to be run in a particular order to pass. (2) The assertion made by a test should be tested by one and only one test - that is, two tests should not break due to the same underlying reason.

The first one is great advice - in fact, you should run your tests in a random order if you can to ensure this. This allows them to be run in parallel as well, which helps make them **Fast**!

The second one seems like good advice on the surface - after all, if only one test fails when a certain piece of functionality breaks, it makes it easier to figure out what broke! However, this also leads you down the path of heavy dependence on [Test Doubles](https://martinfowler.com/bliki/TestDouble.html) like Spies and Mocks. If you are a heavy user of Mocks, don't get mad at me - but they lead to isolated, interaction-based tests. These are reliable and fast, but they end up only testing that the current implementation is in fact, the current implementation. This makes refactoring hard, and these tests can miss bugs that occur _between_ components, requiring broader tests like integration or end-to-end tests to catch those issues.

To better understand this, let's compare and contrast the following three aspects of your tests.

---

#### **Narrow <=> Broad**

Broad tests (like e2e) check multiple pieces of functionality, and are slow, brittle, and flaky. Narrow tests check a single piece of functionality, and are simpler and faster.

#### **Isolated <=> Sociable**

Isolated tests replace the dependencies with test doubles (like mocks), which requires broad (integration) tests to confirm the system works as a whole. Sociable tests use real dependencies, and therefore verify interactions between components.

#### **Interaction-based <=> State-based**

Interaction-based tests check how each dependency is used (like mock verifications). State-based tests check the output or state of the code under test.

---

If you're a frequent mock user, you're probably used to writing Narrow, Isolated, Interaction-based unit tests. My recommendation is, instead, to forgo the mocks and write Narrow, Sociable, State-based tests. This advice comes from the excellent work by James Shore, [Testing Without Mocks](https://www.jamesshore.com/v2/projects/nullables).

### Arrange, Act, Assert

✅ **Recommendation**: Utilize Arrange/Act/Assert comments to organize the parts of your test.

🔎 **Reason**: Consistently including these comments in every test encourages the developer to think about which part of the test they are writing, as well as helps indicate if any of the parts are getting too large and would benefit from a helper method.

Note: If you are asserting that an exception is thrown from the method under test, the Act and Assert sections may be on the same line (e.g. `Assert.Throws(()=> myClass.MyMethod())`). In this case, you should combine the comments like `// Act/Assert`.

## Project Structure

### Test Location

✅ **Recommendation**: Put your test cases right next to the production code files if your language/framework/coworkers will allow (e.g. myClass.js and myClass.spec.js)

🔎 **Reason**: This makes it trivial to find your tests, to find files missing tests, and to move the tests if you move the production file.

➡️ **Alternative**: Create a corresponding test project for every production project, and mirror your folder structure in the test project. This is more conventional in .NET and Java, and I won't blame you for wanting to stick with convention.

### Test Files

✅ **Recommendation**: Create one test file per file under test.

🔎 **Reason**: I've previously recommended creating one test file per _method_ being tested - e.g. for a Repository you'd have something like `SaveTests.cs`, `FindTests.cs`, `DeleteTests.cs`, along with a `MyRepoTestBase.cs` for the shared setup between them. This results in small and organized test files, however, this requires N+1 files for a class with N methods, as well as a folder to separate them from tests for other classes (and then what do you name that folder? Naming it just the name of the class can cause namespace conflicts in .NET, so you would want to append "Tests" to the end, and that's just a lot of prefixing things with "Test".).

Therefore, one test file per production file should be preferred. Your production classes shouldn't have so many methods that your test files become huge and unwieldy after all.

## File Structure

❓Use a base class? Base class could include a generic type parameter to indicate the class under test, and an abstract method to implement that creates an instance of the class under test, for consistency between tests. Base class could be different between integration tests and unit tests?

### Test Case Names

**Recommendation**:

✅ **When using test frameworks with BDD style `describe()/it()` functions**: Use `describe()` to create a group for each function under test, as well as any relevant subgroups to indicate the functionality being tested in a set of tests, and use `it()` to define the scenario being tested for a particular test.

🔎 **Reason**: TODO: It's conventional!

✅ **When using xUnit-style test frameworks**: Use `MethodName_ScenarioUnderTest_ExpectedResult` for test case names.

🔎 **Reason**: The typical advice is to use the `Given_When_Then` syntax, but I find that I frequently don't have any notable starting state to include in the `Given` section, so I combine `Given` and `When` into `ScenarioUnderTest`. Prefixing the test case with the method name is useful to differentiate which method is under test, and sorts nicely in your test runner.

### Multiple Asserts

✅ **Recommendation**: Prefer using one assert per test method. If you want to assert multiple closely-related values, use an assertion block like NUnit's `Assert.Multiple`. If you are asserting multiple properties of a single object, use a comparison library (e.g. [CompareNetObjects](https://github.com/GregFinzer/Compare-Net-Objects)) to compare the whole object and provide a single message listing all of the differences found.

🔎 **Reason**: Multiple assertions can make it harder to track down exactly what went wrong with a test, but repeating the same test multiple times to assert very closely related properties can be messy. Multiple assertion blocks and object comparison libraries prevent the test from short-circuiting after the first failed assertion.

### The three kinds of tests

✅ Each of your classes falls into one of the following categories, and should follow the rules described therein.

#### Pure logic

Super simple unit tests

#### Direct infrastructure dependencies

Integration test these, create nullables

#### Indirect infrastructure dependencies

Narrow, overlapping sociable tests, with nullable infrastructure
