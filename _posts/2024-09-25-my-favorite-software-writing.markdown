---
layout: post
title:  "My Favorite Software Writing"
date: "3024-09-25"
---

## My Favorite Software Writing

There is a lot of good writing about software development and engineering out there. Contained within is a list of my all-time favorite articles, blog posts, books, and etc on the topic. This will be a constantly updated list, so check back occasionally!

I've broken this list into three sections:

- **Life-Changing**: This is the stuff that every software developer should read - **no exceptions**.
- **Important**: Also stuff that I think every developer should read, but read it after the stuff in the life-changing list.
- **Impactful**: Still excellent reads, but might be more situational.

## **Life-changing**

### [Robert C. Martin - Clean Code](https://www.goodreads.com/book/show/3735293-clean-code)

It's hard to think of any other book that I would recommend before this one to another engineer. From decomposing functions, to the importance of formatting, to Test Driven Development, this book covers all of the essentials needed to understand what clean code looks like.

### [Robert C. Martin - The Clean Coder](https://www.goodreads.com/book/show/10284614-the-clean-coder)

This one caught me off guard at first - I was just expecting more highly-technical advice like the first book, but instead it focuses more on the concepts of software craftsmanship, professionalism, and soft skills. Turns out those are just as important as technical skills! Don't get me wrong, this book still has plenty of technical skill advice, just not as much as the first one.

_**Important Note**: While "Uncle" Bob Martin's writing is absolutely essential reading for every software engineer, he presents his views in a way that make them seem like they are always the only "true way" of doing things. While they may represent an ideal state to strive for, pragmatism is also an essential skill for software engineers._

### [Michael C. Feathers - Working Effectively with Legacy Code](https://www.goodreads.com/book/show/44919.Working_Effectively_with_Legacy_Code)

I hate to be the bearer of bad news, but _Working Effectively With Legacy Code_ is a book about testing! Sooner or later every engineer will have to deal with legacy code, and this book takes the bold approach of defining "Legacy Code" as any code without tests. This book is excellently laid out, with each chapter representing a problem that you might be having, such as "I Don’t Have Much Time and I Have to Change It" or "This Class Is Too Big and I Don’t Want It to Get Any Bigger", and then walking you through the process of fixing your problem. It's one of the rare books that works both as a sequential read and a reference guide.

### [Gang of Four - Design Patterns](https://www.goodreads.com/book/show/85009.Design_Patterns)

### [Eric Evans - Domain Driven Design](https://www.goodreads.com/book/show/179133.Domain_Driven_Design)

### [James Shore - Testing Without Mocks](https://www.jamesshore.com/v2/projects/nullables/testing-without-mocks)

### [Joel on Software - Things You Should Never Do, Part I](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/)

### [Martin Fowler - Refactoring](https://www.goodreads.com/book/show/44936.Refactoring)

https://agilemanifesto.org/

https://www.joelonsoftware.com/2000/08/09/the-joel-test-12-steps-to-better-code/

---

## Important

### [Robert C. Martin - Clean Architecture](https://www.goodreads.com/book/show/18043011-clean-architecture)

TODO: Clean architecture summary.

### Reclaim Unreasonable Software

[The Iceberg Secret Revealed](https://www.joelonsoftware.com/2002/02/13/the-iceberg-secret-revealed/)

https://lethain.com/reclaim-unreasonable-software/

- https://lethain.com/testing-strategy-iterative-refinement/

The Pragmatic Programmer

---

## Impactful

### [Google Testing Blog - Let Code Speak For Itself](https://testing.googleblog.com/2023/12/let-code-speak-for-itself.html)

https://testing.googleblog.com/2024/02/increase-test-fidelity-by-avoiding-mocks.html

https://www.jamesshore.com/v2/blog/2023/the-problem-with-dependency-injection-frameworks

https://mcfunley.com/choose-boring-technology

https://increment.com/testing/i-test-in-production/

https://www.honeycomb.io/blog/deploys-wrong-way-change-user-experience

## Way more reading

If you like my recommendations on software writing, check out these other lists of reading materials!

- https://awesome-architecture.com/
