# The DevBytes Blog

## Running locally

`bundle exec jekyll serve`

## Building for prod

`bundle exec jekyll build`

## Topic ideas

- Putting methods on to objects
- Primitive Obsession
- Organizing by type of thing instead of feature